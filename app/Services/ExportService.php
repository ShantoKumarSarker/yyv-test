<?php
namespace App\Services;
use App\Models\AddTrack;
use Excel;
use Illuminate\Http\Request;



class ExportService{

    public function exportAddTrack($rows){
        Excel::create('Add Track List', function($excel) use($rows) {
            $excel->sheet('Add Track List', function($sheet) use($rows) {
                $a = '<table>
                        <tr>
                            <th>SL</th>
                            <th>Image</th>
                            <th>Advertiser</th>
                            <th>Publisher</th>
                            <th>Platform</th>
                            <th>Vertical</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Landing Page</th>
                        </tr>';

                $index = 1;
                foreach ($rows as $r) {
                    $a .= ' <tr>
                            <td>' . $index++ . '</td>
                            <td>' . $r->ImageUrl . '</td>
                            <td>' . $r->advertiserName . '</td>
                            <td>' . $r->PlatformType . '</td>
                            <td>' . $r->category_name . '</td>
                            <td>' . date('Y-m-d', strtotime($r->TimeStamp)) . '</td>
                            <td>' . date('H:i:s', strtotime($r->TimeStamp)) . '</td>
                            <td>' . $r->DestinationLink . '</td>
                            </tr>';
                }
                $a .= '</table>';
                $sheet->loadView('excel', compact('a'));
            });
        })->export('xlsx');
    }
}
