<?php

namespace App\Exports;

use App\Models\AddTrack;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ReportExport implements FromView
{
    public function view(): View
    {
        return view('excel', [
            'rows' => AddTrack::all()
        ]);
    }
}
