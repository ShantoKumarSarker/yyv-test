<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddTrack extends Model
{
    use HasFactory;

    protected $table = "adtrack_test";

    protected $fillable = [
        'TimeStamp',
        'PlatformType',
        'DeviceModel',
        'ImageName',
        'ImageUrl',
        'UserId',
        'CompanyId',
        'BrandName',
        'Name',
        'is_youtube',
        'GDriveLink',
        'FirstName',
        'LastName',
        'category_name',
        'advertiserName'
    ];

}
